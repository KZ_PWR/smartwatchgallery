package pl.zyper.smartwatchgallery

import android.graphics.drawable.Drawable
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

class GalleryPagerAdapter : PagerAdapter() {
    val drawables = intArrayOf(
            R.drawable.nature1,
            R.drawable.nature2,
            R.drawable.nature3,
            R.drawable.nature4,
            R.drawable.nature5)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return drawables.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        Log.i("ADAPTER", "Position: $position")

        val image = ImageView(container.context)
        image.setImageDrawable(container.context.getDrawable(drawables[position]))
        container.addView(image)

        return image
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}